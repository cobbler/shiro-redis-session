
使用redis作为shiro实现集群会话管理，并可配置ehcache作为进程内缓存，通过redis消息订阅发布实现缓存统一

shiro-redis-session
	jar
shiro-redis-session-webapp
	示例demo
	
	
项目依赖：
	shiro 1.2.3+
	spring data redis 1.6.0+
	
	
	
github 地址
	https://github.com/xiaolongyuan/shiro-redis-session

oschina 地址
	http://git.oschina.net/xiaolongyuan/shiro-redis-session